import React from "react"
import { Meta } from "@storybook/react/types-6-0"
import { Story } from "@storybook/react"
import Toast, { ToastProps } from './Toast'

export default {
    title: 'Components/Toast',
    component: Toast
}

const Template: Story<ToastProps> = (args) => <Toast { ...args } />

export const Default = Template.bind({})
