import React from "react";
import { Meta } from "@storybook/react/types-6-0";
import { Story } from "@storybook/react";
import InputError, { InputErrorProps } from "./InputError";

export default {
  title: "Components/Form/InputError",
  component: InputError,
} as Meta;

// Create a master template for mapping args to render the Button component
const Template: Story<InputErrorProps> = (args) => <InputError {...args} />;

// Reuse that template for creating different stories
export const SingleError = Template.bind({});
SingleError.args = { errors: ["Test d'erreur"] };

export const MultipleError = Template.bind({});
MultipleError.args = { errors: ["Test d'erreur", "Test d'erreur 2"] };
