import React, { ReactChildren, ReactNode } from "react";
import "./card.css";

export interface CardProps {
  children:ReactNode;
  rounded?: boolean;
};

/**
 * Primary UI component for user interaction
 */
const Card = ({
  children,
  rounded = true,
}: CardProps) => {
  return (
    <div className={["card", `card--${rounded ? 'rounded' : 'square'}`].join(" ")}
    >
      {children}
    </div>
  );
};

export default Card;
